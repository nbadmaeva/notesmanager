# Author: badmaeva.natalia@gmail.com

from django.conf.urls import patterns, include, url
from notes import views

urlpatterns = patterns('',

    #url(r'^$', views.index, name = 'index'),
    #url(r'^(?P<note_id>\d+)/$', views.detail, name = 'detail'),

    #url(r'^edit/(\d+)/$', views.edit, name = 'edit'),
    #url(r'^add/$', views.add, name = 'add'),
    url(r'^delete/(\d+)/$', views.delete, name = 'delete'),
    #url(r'^add_view/$', views.add_view, name = 'add_view'),

    #url(r'^(?P<note_uuid>[^/]+)/$', views.show_body, name = 'show_body'),

    ##url(r'^login/$', views.login_view, name='login_view'),
)
