# Author: badmaeva.natalia@gmail.com

from django import forms
from django.db import models
from notes.models import Note

class AddNoteForm(forms.ModelForm):
    class Meta:
	model = Note
