# Author: badmaeva.natalia@gmail.com

from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
import uuid
from tinymce import models as tinymce_models

NOTES_CATEGORY = (
	('NOTE', 'Note'),
	('LINK', 'Link'),
	('MEMO', 'Memo'),
	('TODO', 'TODO')
    )    

'''
class Category(models.Model):
    NOTE = 'NT'
    LINK = 'LN'
    MEMO = 'MM'
    TODO = 'TD'

    CHOICES = (
	(NOTE, 'Note'),
	(LINK, 'Link'),
	(MEMO, 'Memo'),
	(TODO, 'TODO')
    )    
    
    category = models.CharField(max_length=4, choices=CHOICES, default='NOTE')
'''

class Note(models.Model):
    uuid = models.CharField(max_length=100, blank=True, unique=True, default=uuid.uuid4, editable=False)
    title = models.CharField(default='Title', max_length=60)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    body = tinymce_models.HTMLField(default='Your Text')
    category = models.CharField(max_length=4, choices=NOTES_CATEGORY, default='NOTE')
    #category = Category(max_length=2, choices=NOTES_CATEGORY, default='Note')
    isFavorite = models.BooleanField(default=False)
    owner = models.ForeignKey(User)

    #def get_absolute_url(self):
#	return reverse('notes-view', kwargs={'pk': self:id})

