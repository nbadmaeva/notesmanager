# Author badmaeva.natalia@gmail.com

from django.contrib import admin
from notes.models import Note

class NoteAdmin(admin.ModelAdmin):
    list_display = ('title', 'created', 'modified', 'body')
    list_filter = ['created', 'modified', 'title']
    search_fields = ['title', 'body']

admin.site.register(Note, NoteAdmin)

