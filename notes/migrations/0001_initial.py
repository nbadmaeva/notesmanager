# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uuid', models.CharField(default=uuid.uuid4, unique=True, max_length=100, blank=True)),
                ('title', models.CharField(default=b'Title', max_length=60)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('body', tinymce.models.HTMLField(default=b'Your Text')),
                ('category', models.CharField(default=b'Note', max_length=1, choices=[(b'Note', b'Note'), (b'Link', b'Link'), (b'Memo', b'Memo'), (b'TODO', b'TODO')])),
                ('isFavorite', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
