# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0002_auto_20141024_1517'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='category',
            field=models.CharField(default=b'NOTE', max_length=4, choices=[(b'NOTE', b'Note'), (b'LINK', b'Link'), (b'MEMO', b'Memo'), (b'TODO', b'TODO')]),
        ),
    ]
