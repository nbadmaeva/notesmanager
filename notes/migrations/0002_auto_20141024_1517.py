# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='uuid',
            field=models.CharField(default=uuid.uuid4, unique=True, max_length=100, editable=False, blank=True),
        ),
    ]
