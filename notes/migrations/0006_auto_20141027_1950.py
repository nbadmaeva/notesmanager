# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0005_auto_20141026_1535'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usernotes',
            name='note',
        ),
        migrations.RemoveField(
            model_name='usernotes',
            name='user',
        ),
        migrations.DeleteModel(
            name='User',
        ),
        migrations.DeleteModel(
            name='UserNotes',
        ),
        migrations.RenameField(
            model_name='note',
            old_name='timestamp',
            new_name='created',
        ),
        migrations.AddField(
            model_name='note',
            name='modified',
            field=models.DateTimeField(default=datetime.date(2014, 10, 27)),
            preserve_default=False,
        ),
    ]
