# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0006_auto_20141027_1950'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='modified',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
