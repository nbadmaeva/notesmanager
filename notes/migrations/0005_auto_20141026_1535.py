# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0004_user_usernotes'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usernotes',
            old_name='noteID',
            new_name='note',
        ),
        migrations.RenameField(
            model_name='usernotes',
            old_name='userID',
            new_name='user',
        ),
    ]
