# Author: badmaeva.natalia@gmail.com

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, render_to_response
#from django.contrib import auth
from django.contrib.auth.models import User
#from django.contrib.auth authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.validators import validate_email
from datetime import datetime

from notes.models import Note
#from notes.forms import AddNoteForm

from django.core.urlresolvers import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

categories = ["Note", "Link", "Memo", "TODO"]

def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username = username, password = password)
    if user is not None:
	if user.is_active:
    	    login(request, user)
	    return HttpResponseRedirect("/notes/")
	else:
	    return HttpResponse("Your account is desabled")
    else:
        return HttpResponse("Incorrect username or password")

def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/loggedout/")

#@login_required(login_url='/login/')
def index(request):
    #if not request.user.is_authenticated():
	#return HttpResponseRedirect("/login/")
    #else:
#	if(request.GET.get('add')):
#	    return render(request, 'notes/add.html', { 'categories': categories })
    
	#latest_notes_list = Note.objects.order_by('-created')[:5]
	latest_notes_list = Note.objects.filter(owner = request.user).order_by('-created')
	context = {'latest_notes_list': latest_notes_list}
	return render(request, 'notes/index.html', context)

#@login_required(login_url='/login/')
def detail(request, note_id):
#    if not request.user.is_authenticated():
#	return HttpResponseRedirect("/login/")
#    else:
	note = get_object_or_404(Note, pk = note_id)
        return render(request, 'notes/detail.html', 
		{'note': note, 
		 'categories': categories,
		 'isFavorite': note.isFavorite
		})

#@login_required(LOGIN_URL)
def show_body(request, note_uuid):
#    if not request.user.is_authenticated():
#	return HttpResponseRedirect("/login/")
#    else:
	note = Note.objects.get(uuid = note_uuid)
	return render(request, 'notes/publish.html', { 'note': note })

#@login_required(login_url='/login/')
def edit(request, note_id):
#    if not request.user.is_authenticated():
#	return HttpResponseRedirect("/login/")
#    else:
	if request.method == 'POST':
    	    note = Note.objects.get(pk = note_id)
	    note.title = request.POST['title']
	    note.body = request.POST['body']
	    note.category = request.POST['category']
	    note.isFavorite = request.POST['isFavorite']
	    note.modified = datetime.now()
	    note.save()
	    return HttpResponse('success')

#@login_required(login_url='/login/')
def add(request):
#    if not request.user.is_authenticated():
#	return HttpResponseRedirect("/login/")
#    else:
	if request.method == 'POST':
	    note = Note.objects.create(
		created = datetime.now(),
		modified = datetime.now(),
    		title = request.POST['title'],
		body = request.POST['body'],
		category = request.POST['category'],
		isFavorite = request.POST['isFavorite'],
		owner = request.user
	    )
	    return HttpResponse('success')

#@login_required(login_url='/login/')
def delete(request, note_id):
#    if not request.user.is_authenticated():
#	return HttpResponseRedirect("/login/")
#    else:
	if request.method == 'POST':
	    note = Note.objects.get(pk = note_id)
	    note.delete()
	    return HttpResponse('success')
	    #return reverse('notes-list')  

#@login_required(login_url='/login/')
def add_view(request):
    return render(request, 'notes/add.html',
	{ 'categories': categories })
 

class ListNoteView(ListView):
    model = Note
    template_name = 'notes/notes_list.html'
    
    def get_queryset(self):
	return Note.objects.filter(owner = self.request.user).order_by('-created')

class CreateNoteView(CreateView):
    model = Note
    template_name = 'notes/edit_note.html'

    def get_success_url(self):
        return reverse('notes-list')

    def get_context_data(self, **kwargs):
	context = super(CreateNoteView, self).get_context_data(**kwargs)
	context['action'] = reverse('notes-add')
	return context

class UpdateNoteView(UpdateView):
    model = Note
    template_name = 'notes/edit_note.html'

    def get_success_url(self):
	return reverse('notes-list')

    def get_context_data(self, **kwargs):
	context = super(UpdateNoteView, self).get_context_data(**kwargs)
	context['action'] = reverse('notes-edit', kwargs={'pk': self.get_object().id})
	return context

class DeleteNoteView(DeleteView):
    model = Note
    template_name = 'notes/delete_note.html'

    def get_success_url(self):
	return reverse('notes-list')  
