# Author: badmaeva.natalia@gmail.com

from django.conf.urls import patterns, include, url
from django.contrib import admin
from notesmanager.settings import STATIC_ROOT

import notes.views

urlpatterns = patterns('',
    #url(r'^$', 'notes.views.index'),
    url(r'^$', notes.views.ListNoteView.as_view(), name = 'notes-list',),
    url(r'^add/$', notes.views.CreateNoteView.as_view(), name = 'notes-add',),
    url(r'^edit/(?P<pk>\d+)/$', notes.views.UpdateNoteView.as_view(), name = 'notes-edit',),
    #url(r'^delete/(?P<pk>\d+)/$', notes.views.DeleteNoteView.as_view(), name = 'notes-delete',),

    url(r'^notes/', include('notes.urls', namespace = 'notes')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    #url(r'^login/$', include('notes.urls', namespace = 'notes')),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),

    url(r'^static/(.*)$', 'django.views.static.serve', 
	{'document_root': STATIC_ROOT, 'show_indexes': True}),
)
